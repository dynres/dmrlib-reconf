#include "dmr.h"

void DMR_Send_shrink(double *data, int size, MPI_Comm DMR_INTERCOMM)
{
    // printf("(sergio): %s(%s,%d)\n", __FILE__, __func__, __LINE__);
    int my_rank, comm_size, intercomm_size, factor, dst;
    MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
    MPI_Comm_size(MPI_COMM_WORLD, &comm_size);
    MPI_Comm_remote_size(DMR_INTERCOMM, &intercomm_size);
    factor = comm_size / intercomm_size;
    dst = my_rank / factor;
    MPI_Send(data, size, MPI_DOUBLE, dst, 0, DMR_INTERCOMM);
}

void DMR_Recv_shrink(double **data, int *size, MPI_Comm DMR_INTERCOMM)
{
    // printf("(sergio): %s(%s,%d)\n", __FILE__, __func__, __LINE__);
    int my_rank, comm_size, parent_size, src, factor, iniPart, i, parent_data_size;
    MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
    MPI_Comm_size(MPI_COMM_WORLD, &comm_size);
    MPI_Comm_remote_size(DMR_INTERCOMM, &parent_size);
    MPI_Status status;
    factor = parent_size / comm_size;
    *data = (double *)malloc((*size) * sizeof(double));
    for (i = 0; i < factor; i++)
    {
        src = my_rank * factor + i;
        iniPart = parent_data_size * i;
        MPI_Recv((*data) + iniPart, (*size), MPI_DOUBLE, src, 0, DMR_INTERCOMM, &status);
    }
}

void DMR_Send_expand(double *data, int size, MPI_Comm DMR_INTERCOMM)
{
    // printf("(sergio): %s(%s,%d): %d\n", __FILE__, __func__, __LINE__, size);
    int my_rank, comm_size, intercomm_size, localSize, factor, dst;
    MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
    MPI_Comm_size(MPI_COMM_WORLD, &comm_size);
    MPI_Comm_remote_size(DMR_INTERCOMM, &intercomm_size);
    factor = intercomm_size / comm_size;
    localSize = size / factor;
    for (int i = 0; i < factor; i++)
    {
        dst = my_rank * factor + i;
        int iniPart = localSize * i;
        // printf("(sergio): %s(%s,%d): %d send to %d (%d)\n", __FILE__, __func__, __LINE__, my_rank, dst, localSize);
        MPI_Send(data + iniPart, localSize, MPI_DOUBLE, dst, 0, DMR_INTERCOMM);
    }
}

void DMR_Recv_expand(double **data, int *size, MPI_Comm DMR_INTERCOMM)
{
    // printf("(sergio): %s(%s,%d): %d\n", __FILE__, __func__, __LINE__, *size);
    int my_rank, comm_size, parent_size, src, factor;
    MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
    MPI_Comm_size(MPI_COMM_WORLD, &comm_size);
    MPI_Comm_remote_size(DMR_INTERCOMM, &parent_size);
    MPI_Status status;
    factor = comm_size / parent_size;
    *data = (double *)malloc((*size) * sizeof(double));
    src = my_rank / factor;
    // printf("(sergio): %s(%s,%d): %d recv from %d (%d)\n", __FILE__, __func__, __LINE__, my_rank, src, *size);
    MPI_Recv(*data, *size, MPI_DOUBLE, src, 0, DMR_INTERCOMM, &status);
}

int DMR_Reconfiguration(char *argv[], MPI_Comm DMR_COMM, MPI_Comm *DMR_INTERCOMM, int *DMR_STATE, int min, int max, int step, int pref)
{
    int world_size, world_rank;
    MPI_Comm_size(DMR_COMM, &world_size);
    MPI_Comm_rank(DMR_COMM, &world_rank);
    int action = 1, nc = world_size+1;
    int *errcodes = (int *) malloc(sizeof(int) * nc);
    job_desc_msg_t job_update;
    job_info_msg_t *MasterInfo;
    char *jID = getenv("SLURM_JOBID");
    uint32_t jobID = atoi(jID);
    char *finalNodelist = (char *)malloc(sizeof(char) * STR_SIZE);
        if (world_rank == 0)
        {
            // printf("@@@[%d]-> %s(%s,%d)\n", getpid(), __func__, __FILE__, __LINE__);
            slurm_load_job(&MasterInfo, jobID, 0);
            if (MasterInfo == NULL || MasterInfo->record_count == 0)
            {
                slurm_perror((char *)"%%%%%% No info %%%%%%");
                exit(1);
            }

            char currHostnames[2048], appendHostnames[2048];
	    appendHostnames[0]='\0';
            readableHostlist(MasterInfo->job_array[0].nodes, currHostnames);
            sprintf(finalNodelist, "%s%s", currHostnames, appendHostnames);
            slurm_free_job_info_msg(MasterInfo);
        }
    MPI_Bcast(finalNodelist, STR_SIZE + 1, MPI_CHAR, 0, DMR_COMM);

    MPI_Info *host_info = (MPI_Info*) malloc(sizeof(MPI_Info));
    MPI_Info_create(host_info);
    MPI_Info_set(*host_info, "host", finalNodelist);
    MPI_Info_set(*host_info, "map_by", "node");

    //printf("(sergio): %s(%s,%d) nc %d %s\n", __FILE__, __func__, __LINE__, nc, finalNodelist);
    malleability_init_spawn(argv[0], nc, 0, DMR_COMM);
    //malleability_spawn_set_mapping(*host_info);
    //malleability_spawn_set_method(MALL_SPAWN_MERGE);
    //malleability_spawn_add_strats(MALL_SPAWN_SINGLE);
    malleability_spawn_add_strats(MALL_SPAWN_PTHREAD);
    //malleability_spawn_add_strats(MALL_SPAWN_SINGLE*MALL_SPAWN_PTHREAD);
    *DMR_STATE = malleability_start_spawn(DMR_INTERCOMM);
    // printf("(sergio): %s(%s,%d)\n", __FILE__, __func__, __LINE__);

    return action;
}

void DMR_Reconfiguration_check(MPI_Comm DMR_COMM, MPI_Comm *DMR_INTERCOMM, int *DMR_STATE) {
  *DMR_STATE = malleability_check_spawn_state(DMR_INTERCOMM, DMR_COMM);
  if(*DMR_STATE == MALL_SPAWN_ADAPT_POSTPONE) *DMR_STATE = MALL_SPAWN_ADAPT_PENDING; //FIXME Remove when distributing data with Merge
}
void DMR_Reconfiguration_children(MPI_Comm DMR_COMM, MPI_Comm *DMR_INTERCOMM) {
  int world_size, world_rank, world_root = 0, is_intercomm;
  MPI_Comm_size(DMR_COMM, &world_size);
  MPI_Comm_rank(DMR_COMM, &world_rank);
  malleability_connect_children(world_rank, world_size, world_root, DMR_COMM, DMR_INTERCOMM);
}
