#!/bin/bash

##SBATCH --job-name=dmr
#SBATCH --output=slurm-dmr_%j.out
#SBATCH --error=slurm-dmr_%j.err

#echo $SLURM_NNODES
#echo $SLURM_NODELIST
#echo $SLURM_JOBID

export LD_LIBRARY_PATH=/home/iker/Instalaciones/slurm-spawn/lib:/home/iker/Instalaciones/openmpi-4.1.4/lib:/home/iker/Documentos/dmr:$LD_LIBRARY_PATH
export PATH=/home/iker/Instalaciones/slurm-spawn/bin:$PATH

NODELIST="$(scontrol show hostname $SLURM_JOB_NODELIST | paste -d, -s)"
#echo $NODELIST

echo $SLURM_JOB_NUM_NODES
echo $NODELIST
#OMPI RUN
mpirun -oversubscribe -np $SLURM_JOB_NUM_NODES ./sleepOf
#MPICH RUN
#mpirun -launcher ssh -np $SLURM_JOB_NUM_NODES ./sleepOf
