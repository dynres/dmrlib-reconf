#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <unistd.h>
#include <math.h>
#include <string.h>

#include "dmr.h"
//#include "spawnlib.h"

#define DURATION 1
#define STEPS 10
//#define SIZE 1024
// #define SIZE 131072
#define SIZE 268435456 // 2^28
// #define SIZE 4294967296 //2^32 ->  MPI_Send(174)................: MPI_Send(buf=0x2af34c9e3860, count=1, dtype=USER<struct>, dest=12, tag=1200, comm=0x84000000) failed

#define BLOCKSIZE 128
#define NBLOCKS SIZE / BLOCKSIZE

void initialize_data(int size, double **vector)
{
    (*vector) = (double *)malloc(size * sizeof(double));
    for (int i = 0; i < size; i++)
    {
        (*vector)[i] = i * 2;
    }
}

void compute(int i, int world_size, int world_rank, int bytes)
{
    int durStep = DURATION / world_size;
    durStep = 1; // TODO Delete
    if (world_rank == 0)
        printf("[%d/%d] COMPUTE: %s(%s,%d) - Step %d doing a sleep of %d seconds (bytes per rank %d)\n", world_rank, world_size, __FILE__, __func__, __LINE__, i, durStep, bytes);
     sleep(durStep);
}

// USAGE ./sleepOf
int main(int argc, char **argv)
{
    // printf("(sergio): %s(%s,%d)  USAGE mpirun -n <np> ./sleepOf\n", __FILE__, __func__, __LINE__);
    int provided;
    MPI_Init_thread(&argc, &argv, MPI_THREAD_MULTIPLE, &provided);
    if(provided != MPI_THREAD_MULTIPLE) {
      printf("MPI does not provide MPI_THREAD_MULTIPLE capability (provided=%d)\nAborting\n", provided);
      MPI_Finalize();
      exit(0);
    }

    int world_rank, world_size;
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);

    int len;
    char name[MPI_MAX_PROCESSOR_NAME];
    MPI_Get_processor_name(name, &len);
    printf("(sergio): %s(%s,%d)  %d/%d in %s\n", __FILE__, __func__, __LINE__, world_rank, world_size, name);
    fflush(stdout);

    int localSize = SIZE / world_size;
    double *vector;

    DMR_RECONFIGURATION(STEPS, initialize_data(localSize, &vector), compute(i, world_size, world_rank, localSize));

    MPI_Finalize();
    return 0;
}
