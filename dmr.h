#include <stdio.h>
#include <stdlib.h>
#include "mpi.h"
#include <unistd.h>
#include "slurm/slurm.h"
#include <string>
#include <vector>
#include <sys/types.h>
#include <signal.h>
#include <iostream>
#include "spawn-methods/GenericSpawn.h"
//#include "spawn-methods/SpawnDatatypes.h"
#define DMR_filename "tmp"
#define STR_SIZE 2048           /**< @brief Standard size for strings. **/

/**
 * @brief Struct to send/receive information with Slurm. 
 */
typedef struct select_jobinfo {
    uint32_t job_id; /**< @brief IN Job identifier. */
    uint32_t action; /**< @brief OUT Action identifier. */
    uint32_t min; /**< @brief IN Minimum number of processes. */
    uint32_t max; /**< @brief IN Maximum number of processes. */
    uint32_t preference; /**< @brief IN Preferred number of processes. */
    uint32_t step; /**< @brief IN Multiple of number of processes. */
    uint32_t currentNodes; /**< @brief IN Current number or processes. */
    uint32_t resultantNodes; /**< @brief OUT Resultant number of processes. */
    char hostlist[STR_SIZE]; /**< @brief IN List of hosts currently allocated. This is used to select the manager host for shrinking. */
} _dmr_slurm_jobinfo_t;

#define DMR_DISABLE_OPTION 0
#define DMR_SPAWN_COMPLETED(state) (state == MALL_SPAWN_COMPLETED || state == MALL_SPAWN_ADAPTED || state == MALL_SPAWN_ADAPT_POSTPONE)
#define DMR_SPAWN_PENDING(state) (state == MALL_SPAWN_PENDING || state == MALL_SPAWN_SINGLE_PENDING || state == MALL_SPAWN_ADAPT_PENDING)
#define DMR_RECONFIGURATION(DMR_ts, DMR_initialize, DMR_compute) {\
    int DMR_comm_rank, DMR_comm_size, DMR_parent_size, DMR_it, DMR_action, DMR_STATE, DMR_is_intercomm;\
    int DMR_is_pending = 0, DMR_pending_after=0;\
    MPI_Comm DMR_INTERCOMM;\
    DMR_STATE = MALL_NOT_STARTED;\
    DMR_action = 0;\
    \
    DMR_COMM=MPI_COMM_WORLD;\
    MPI_Comm_rank(DMR_COMM, &DMR_comm_rank);\
    MPI_Comm_size(DMR_COMM, &DMR_comm_size);\
    MPI_Comm_get_parent(&DMR_INTERCOMM);\
    if (DMR_INTERCOMM != MPI_COMM_NULL) {\
	DMR_Reconfiguration_children(DMR_COMM, &DMR_INTERCOMM);\
	MPI_Comm_test_inter(DMR_INTERCOMM, &DMR_is_intercomm);\
        if (DMR_is_intercomm && DMR_DISABLE_OPTION) {\
          MPI_Comm_remote_size(DMR_INTERCOMM, &DMR_parent_size);\
          if (DMR_comm_size > DMR_parent_size) DMR_Recv_expand(&vector, &localSize, DMR_INTERCOMM);\
          else DMR_Recv_shrink(&vector, &localSize, DMR_INTERCOMM);\
	  MPI_Comm_disconnect(&DMR_INTERCOMM);\
	}\
        if (DMR_is_intercomm) {\
          MPI_Barrier(DMR_INTERCOMM);\
	  MPI_Comm_disconnect(&DMR_INTERCOMM);\
	} else {\
	  MPI_Comm_dup(DMR_INTERCOMM, &DMR_COMM);\
	  MPI_Comm_free(&DMR_INTERCOMM);\
          MPI_Comm_rank(DMR_COMM, &DMR_comm_rank);\
          MPI_Comm_size(DMR_COMM, &DMR_comm_size);\
	  world_rank = DMR_comm_rank; world_size = DMR_comm_size;\
	}\
    }\
    \
    if (access(DMR_filename, F_OK) == 0) {\
        DMR_it = read_state();\
	MPI_Barrier(MPI_COMM_WORLD);\
    } else {\
        DMR_it = 0;\
        DMR_initialize;\
    }\
    \
	printf("PRE %d\n", DMR_it);\
    for (int i = DMR_it; i < DMR_ts; i++){\
	MPI_Barrier(DMR_COMM);\
        DMR_compute;\
        if(world_rank == 0){\
            save_state(i);\
        }\
	DMR_is_pending = DMR_SPAWN_PENDING(DMR_STATE);\
	if (i+1 != DMR_ts || DMR_is_pending) {\
	  if (!DMR_is_pending) {\
	    DMR_action = DMR_Reconfiguration(argv, DMR_COMM, &DMR_INTERCOMM, &DMR_STATE, 1, 8, 0, 0);\
	  } else DMR_Reconfiguration_check(DMR_COMM, &DMR_INTERCOMM, &DMR_STATE);\
          if (DMR_action && DMR_SPAWN_COMPLETED(DMR_STATE)){\
              if (DMR_action == 1 && DMR_DISABLE_OPTION) DMR_Send_expand(vector, localSize, DMR_INTERCOMM);\
              else if (DMR_action == 2 && DMR_DISABLE_OPTION) DMR_Send_shrink(vector, localSize, DMR_INTERCOMM);\
	      MPI_Comm_test_inter(DMR_INTERCOMM, &DMR_is_intercomm);\
              if (DMR_is_intercomm) {\
		MPI_Barrier(DMR_INTERCOMM);\
                MPI_Comm_disconnect(&DMR_INTERCOMM);\
	        MPI_Finalize();\
	        exit(0);\
	      }\
	      if (DMR_STATE == MALL_SPAWN_ADAPTED) {\
                MPI_Comm_size(DMR_INTERCOMM, &DMR_comm_size);\
	        if (DMR_comm_rank >= DMR_comm_size) {\
	          MPI_Finalize();\
	          exit(0);\
	        }\
	      }\
	      if (DMR_STATE != MALL_SPAWN_ADAPT_PENDING) {\
	        if (DMR_COMM != MPI_COMM_WORLD) MPI_Comm_free(&DMR_COMM);\
	        MPI_Comm_dup(DMR_INTERCOMM, &DMR_COMM);\
	        MPI_Comm_free(&DMR_INTERCOMM);\
                MPI_Comm_rank(DMR_COMM, &DMR_comm_rank);\
                MPI_Comm_size(DMR_COMM, &DMR_comm_size);\
	        world_rank = DMR_comm_rank; world_size = DMR_comm_size;\
	        DMR_STATE = MALL_NOT_STARTED; DMR_action = 0;\
	      }\
          }\
	}\
    }\
    while (DMR_SPAWN_PENDING(DMR_STATE)) {\
      DMR_Reconfiguration_check(DMR_COMM, &DMR_INTERCOMM, &DMR_STATE);\
      sleep(1);\
      DMR_pending_after=1;\
    }\
    if (DMR_INTERCOMM != MPI_COMM_NULL) {\
      MPI_Comm_test_inter(DMR_INTERCOMM, &DMR_is_intercomm);\
      if (DMR_is_intercomm) {\
        MPI_Barrier(DMR_INTERCOMM);\
        MPI_Comm_disconnect(&DMR_INTERCOMM);\
      } else {\
	MPI_Comm_dup(DMR_INTERCOMM, &DMR_COMM);\
	MPI_Comm_free(&DMR_INTERCOMM);\
        MPI_Comm_rank(DMR_COMM, &DMR_comm_rank);\
        MPI_Comm_size(DMR_COMM, &DMR_comm_size);\
	world_rank = DMR_comm_rank; world_size = DMR_comm_size;\
      }\
    }\
    if (world_rank == 0 && !DMR_pending_after) remove(DMR_filename);\
}\

int read_state(){
    int i;
    FILE *DMR_file = fopen (DMR_filename, "r");
    fscanf(DMR_file, "%d", &i);
    fclose(DMR_file);
    return(i);
}

void save_state(int i){
    FILE *DMR_file = fopen (DMR_filename, "w");
    fprintf(DMR_file, "%d", i+1);
    fclose(DMR_file);
}

int get_action(int *action, int *nc, int it){
    if (it%3==1){
        if ((*nc)==128) {//shrink
            *action = 2;
            //*nc = *nc/2;
            *nc=1;    
        } else { //expand
            *action = 1;
            *nc = *nc*2;
        }
    }
    return 0;
}

void readableHostlist(char * nameExp, char * readableNames) {
    hostlist_t hl = slurm_hostlist_create(nameExp);
    char *host = slurm_hostlist_shift(hl);
    sprintf(readableNames, "%s", host);
    host = slurm_hostlist_shift(hl);
    while (host != NULL) {

        sprintf(readableNames, "%s,%s", readableNames, host);
        host = slurm_hostlist_shift(hl);
    }
    slurm_hostlist_destroy(hl);
}

void SLURMbuildHostLists(hostlist_t hl, std::vector<std::string>& tokensParams, std::vector<std::string>& tokensHost, std::vector<int>& hostInstances) {
    std::string params = "none";

    char *host = slurm_hostlist_shift(hl);

    while (host != NULL) {
        hostInstances.push_back(1);
        tokensHost.push_back(host);
        tokensParams.push_back(params);
        host = slurm_hostlist_shift(hl);
    }
}

MPI_Comm DMR_INTERCOMM;
MPI_Comm DMR_COMM;

int DMR_Reconfiguration(char *argv[], MPI_Comm DMR_COMM, MPI_Comm *DMR_INTERCOMM, int *DMR_STATE, int min, int max, int step, int pref);
void DMR_Reconfiguration_check(MPI_Comm DMR_COMM, MPI_Comm *DMR_INTERCOMM, int *DMR_STATE);
void DMR_Reconfiguration_children(MPI_Comm DMR_COMM, MPI_Comm *DMR_INTERCOMM);
void DMR_Send_expand(double *data, int size, MPI_Comm DMR_INTERCOM);
void DMR_Recv_expand(double **data, int *size, MPI_Comm DMR_INTERCOM);
void DMR_Send_shrink(double *data, int size, MPI_Comm DMR_INTERCOM);
void DMR_Recv_shrink(double **data, int *size, MPI_Comm DMR_INTERCOM);
