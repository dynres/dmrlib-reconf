DIR	= /home/iker/Instalaciones
APP = 	/home/iker/Documentos/dmr
DMRFLAGS = -I$(APP) -L$(APP) -ldmr
FLAGS  = -O3 -Wall 
MPIFLAGS    = -I$(DIR)/openmpi-4.1.4/include -L$(DIR)/openmpi-4.1.4/lib -lmpi
SLURMFLAGS  = -I$(DIR)/slurm-spawn/include -L$(DIR)/slurm-spawn/lib -lslurm

all:  clean lib sleep

lib: dmr.c
	g++ $(MPIFLAGS) $(SLURMFLAGS) -c -fPIC dmr.c -o dmr.o
	g++ $(MPIFLAGS) -c -fPIC spawn-methods/Baseline.c -o Baseline.o
	g++ $(MPIFLAGS) -c -fPIC spawn-methods/Merge.c -o Merge.o
	g++ $(MPIFLAGS) -c -fPIC spawn-methods/SpawnThreadState.c -o SpawnThreadState.o
	g++ $(MPIFLAGS) -c -fPIC spawn-methods/GenericSpawn.c -o GenericSpawn.o
	g++ $(MPIFLAGS) $(SLURMFLAGS) -c -fPIC spawn-methods/ProcessDist.c -o ProcessDist.o
	g++ dmr.o Baseline.o Merge.o SpawnThreadState.o GenericSpawn.o ProcessDist.o -shared -o libdmr.so 

sleep: sleepOf.c
	mpic++ sleepOf.c -o sleepOf $(MPIFLAGS) $(FLAGS) $(DMRFLAGS) $(SLURMFLAGS)

clean:
	rm -f *.out *.o core.* *.err *.so tmp sleepOf hostfile.txt
