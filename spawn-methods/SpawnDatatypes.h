#ifndef MALLEABILITY_SPAWN_STATES_H
#define MALLEABILITY_SPAWN_STATES_H

#include <mpi.h>

//States
#define MALL_DENIED -1
enum mall_spawn_states{MALL_UNRESERVED, MALL_NOT_STARTED, MALL_ZOMBIE, MALL_SPAWN_PENDING, MALL_SPAWN_SINGLE_PENDING, 
	MALL_SPAWN_SINGLE_COMPLETED, MALL_SPAWN_ADAPT_POSTPONE, MALL_SPAWN_COMPLETED, MALL_DIST_PENDING, MALL_DIST_COMPLETED, 
	MALL_SPAWN_ADAPT_PENDING, MALL_SPAWN_ADAPTED, MALL_COMPLETED};
enum mall_spawn_methods{MALL_SPAWN_BASELINE, MALL_SPAWN_MERGE};
#define MALL_SPAWN_NO_STRATEGY 1
#define MALL_SPAWN_PTHREAD 2
#define MALL_SPAWN_SINGLE 3

enum mall_spawn_mappings{MALL_SPAWN_DIST_SPREAD, MALL_SPAWN_DIST_COMPACT};
enum mall_spawn_mappings_info_types{MALL_SPAWN_DIST_RMS, MALL_SPAWN_DIST_STRING, MALL_SPAWN_DIST_HOSTFILE};

#define MALLEABILITY_ROOT 0

/*
 * Shows available data structures for inner ussage.
 */

/* --- SPAWN STRUCTURES --- */
struct physical_dist {
  int num_cpus, num_nodes;
  char *nodelist;
  int target_qty, already_created;
  int dist_type, info_type;
};

typedef struct {
  int myId, root, root_parents;
  int spawn_qty, initial_qty, target_qty;
  int already_created;
  int dist_type, info_type;
  int spawn_method, spawn_is_single, spawn_is_async;
  char *argv, *cmd; //Executable namea
  MPI_Info mapping;
  MPI_Datatype dtype;
  struct physical_dist dist; // Used to create mapping var

  MPI_Comm comm, returned_comm;
} Spawn_data;

#endif
