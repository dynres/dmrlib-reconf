#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <mpi.h>
#include "GenericSpawn.h"
#include "SpawnDatatypes.h"

int main(int argc, char *argv[]) { 
  int myId, numP, req, is_intercomm;
  MPI_Comm resize;
  MPI_Init_thread(&argc, &argv, MPI_THREAD_MULTIPLE, &req);
  MPI_Comm_rank(MPI_COMM_WORLD, &myId);
  MPI_Comm_size(MPI_COMM_WORLD, &numP);

  MPI_Comm_get_parent(&resize);
  if(resize == MPI_COMM_NULL ) { 
    init_spawn(argv[0], numP*2, 0, MPI_COMM_WORLD);

    //malleability_spawn_set_method(MALL_SPAWN_MERGE);
    //malleability_spawn_add_strats(MALL_SPAWN_SINGLE);
    //malleability_spawn_add_strats(MALL_SPAWN_PTHREAD);
    //malleability_spawn_add_strats(MALL_SPAWN_PTHREAD * MALL_SPAWN_SINGLE);
    //malleability_spawn_set_mapping(MPI_INFO_NULL);
    int state = start_spawn(&resize);

    while(state != MALL_SPAWN_COMPLETED) {
      sleep(1);
      state = check_spawn_state(&resize, MPI_COMM_WORLD);
    }


  } else {
    int numP_parents, root_parents;
    malleability_connect_children(myId, numP, 0, MPI_COMM_WORLD, &resize);
    printf("We are children %d/%d\n", myId, numP);
  }

  MPI_Comm_test_inter(resize, &is_intercomm);
  if(is_intercomm) {
    MPI_Comm_disconnect(&resize);
  }

  MPI_Finalize();
}
