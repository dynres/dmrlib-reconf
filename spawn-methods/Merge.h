#ifndef MALLEABILITY_SPAWN_MERGE_H
#define MALLEABILITY_SPAWN_MERGE_H

#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include "SpawnDatatypes.h"

int merge(Spawn_data spawn_data, MPI_Comm *child, int data_state);

#endif
