#ifndef MALLEABILITY_GENERIC_SPAWN_H
#define MALLEABILITY_GENERIC_SPAWN_H

#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include "SpawnDatatypes.h"

void malleability_init_spawn(char *argv, int target_qty, int root, MPI_Comm comm);
int malleability_start_spawn(MPI_Comm *child);
int malleability_check_spawn_state(MPI_Comm *child, MPI_Comm comm);
void malleability_connect_children(int myId, int numP, int root, MPI_Comm comm, MPI_Comm *parents);
//void malleability_connect_children(int myId, int numP, int root, MPI_Comm comm, int *numP_parents, int *root_parents, MPI_Comm *parents);

void malleability_spawn_set_children(int target_qty);
void malleability_spawn_set_root(int root);
void malleability_spawn_set_method(int method);
void malleability_spawn_set_mapping(MPI_Info info);
void malleability_spawn_set_mapping_type(int mapping_type);
void malleability_spawn_set_mapping_info_type(int info_type);

void malleability_spawn_add_strats(int strategies);
void malleability_spawn_remove_strats(int strategies);
int malleability_spawn_get_strats(int *result);
int malleability_spawn_contains_strat(int spawn_strategies, int strategy, int *result);


void unset_spawn_postpone_flag(int outside_state);

#endif
